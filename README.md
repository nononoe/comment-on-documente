# comment on documente

[Site](https://felou.gitlab.io/comment-on-documente/) rassemblant du matériel d'observation (photos, scans de carnets, site web, bout de code) sur les pratiques et les fonctions de la documentation.

## Rapport de stage sur les pratiques de documentation au sein du collectif Luuse (février mars 2022).
Luuse est un collectif qui travaille le medium numérique à partir des valeurs du libre pour produire des outils d'édition, d'écriture, de publication et d'archive. dans la production de ces outils, Luuse cultive une approche expérimentale et critique.  
Ce rapport de stage s'intéresse aux pratiques de documentation mises en place par des moyens technologiques et humains, et aux enjeux qu'elles recouvrent.

## Outils
- Template du site produit avec [Mkdocs](https://www.mkdocs.org/)  
- Page bibliographique générée avec le module Pyzotero