# Restituer la recherce

## Restitution évènementielle
Les deux projets du Bookscanner et de l'outil de bibliothèque pirate numérique feront l'objet d'un évènement de restitution au début du mois de mai. L'enjeu est de restituer le processus d'expérimentations ainsi que les enjeux de la recherche autour des artefacts que nous allons présenter.

*Brouillon de mail d'invitation: organiser une scan party ?*

![Scan party](img/mail-evenement.png)

*Brouillons de carte mentale des enjeux soulevés par l'outil de bibliothèque numérique pirate*

![Mind-map brouillon sur carnet](img/mindmap-brouillon.png)

![Mind-map brouillon](img/mapblbio.png)

*Tests pour le module lumière du Bookscanner* 

![Bookscanner](img/bookscanner.jpeg) | ![Bookscanner2](img/bookscanner2.jpeg)
:----------- |:-------------:
![Plaque](img/corde-scan.jpg) | ![Plan](img/plan-plaque.jpg)

## Récupérer Zotero
Au cours de ce travail sur l'outil de bibliothèque numérique pirate, il nous a semblé important de constituer une bibliographie au sujet de la piraterie, des communs et de leur diffusion. Nous avons rassemblé ces ressources afin de nous aider à aborder de manière critique ces enjeux. Au moment de nous constituer cette bibliographie, nous avons récupéré et réadapté un outil de restitution du contenu d'une bibliothèque Zotero (métadonnées, notes, marqueurs).

*Capture d'écran de la [page de bibliographie](http://archivism.luuse.io/piraterie-et-bien-commun/index.html)*

![Bibliographie](img/biblio-pbc.png)

*Récupération des données de Zotero par l'API*

![Récupérer les données de Zotero](img/API-zotero.png)

## Documentation du Bookscanner
Le site de documentation du Bookscanner explique dans le détail toute notre démarche de recherche et nos expérimentations. Thomas a produit une [documentation très détaillée](https://luuse.gitlab.io/recherche_formes-archives/book-scanner/site-documentation/nettoyage%2Bocr/) de son processus de recherche pour le **programme de traitement** des images scannés.

![Documentation de scannickel](img/scannickel.png)