# Permettre la réappropriation
Lors du développement d'un outil libre, la documentation occupe une place essentielle pour autoriser la réappropration de l'outil par des personnes extérieures, qui n'en sont pas auteures. Voilà quelques exemples observés de moyens techniques et humains de documentation qui permette de s'approprier un outil.

## Mkdocs
Pour entamer la documentation de notre bookscanner, nous avons produit un [site](https://luuse.gitlab.io/recherche_formes-archives/book-scanner/site-documentation/) qui utilise, comme celui-ci, l'outil **[Mkdocs](https://www.mkdocs.org/)**. **Mkdocs** est un générateur de site statique spécialement conçu pour produire la documentation d'un projet. Les fichiers sources dans lesquels est écrit le contenu des pages est écrit en langage **Markdown**, un langage de balisage dont la syntaxe est très simple à lire et à écrire.

*Ce paragraphe en Markdown dans le fichier source*

```
## Mkdocs
Pour entamer la documentation de notre bookscanner, nous avons produit un [site](https://luuse.gitlab.io/recherche_formes-archives/book-scanner/site-documentation/) qui utilise, comme celui-ci, l'outil **[Mkdocs](https://www.mkdocs.org/)**. **Mkdocs** est un générateur de site statique spécialement conçu pour produire la documentation d'un projet. Les fichiers sources dans lesquels est écrit le contenu des pages est écrit en langage **Markdown**, un langage de balisage dont la syntaxe est très simple à lire et à écrire.
```

Mkdocs intègre des éléments simples de navigation que l'on peut configurer au moyen d'un fichier de configuration.

*Capture d'écran du fichier de configuration de ce site.*

![Screenshot](img/config-yml.png)


## Un template ouvert
Il est habituel d'utiliser les commentaires pour documenter un code à l'intérieur de lui-même (expliquer la machine là où elle se trouve). On précise ce que font chaque parties pour permettre à une personne non-auteure de les identifier et de les modifier. J'ai fait cet exercice pour un template de page bibliographique (récupérant les données d'une [bibliothèque Zotero](restitution-recherche.md#recuperer-zotero)), dont il était prévu qu'il soit réutilisé et modifié pour d'autres usages. Je précisais donc à quelles données je faisais appel dans chaque partie pour qu'elle puisse gardée ou supprimée.

*Extrait du code*

``` html
<!--  CREATEURICES -->
			{% for creator in doc.data.creators %}
			<ul class="creators">
				<li>
					{{ creator.firstName }} {{ creator.lastName }}
					<!--  type de créateurice (traduction en français avec le filtre replace) -->
					({{ creator.creatorType|replace("author", "auteur.ice")|replace("director", "metteur.euse en scène") |replace("translator", "traducteur.ice") | replace("editor", "éditeur.ice") }})
				</li>
			</ul>
			{% endfor %}

			<!--  MOTS CLES -->
			{% if doc.data.tags | length > 0 %}
			<div class="taglist">
				{% set tags = [] %}
				<span style="display: none">{% for tag in doc.data.tags %}{{ tags.append(tag.tag) }}{% endfor %}</span>
				mots clés : <span class="tag">{{ tags|join (', ') }}</span>

      {% else %}

      <div class="notags">
			{% endif %}

      </div>

			<!--  RESUME -->
			{% if doc.data.abstractNote %}
			<div id="abstractNote">

					<!--  si moins de 30 mots afficher en entier -->
					{% if doc.data.abstractNote|wordcount < 30 %}
						<div class="boite-courte">résumé : {{doc.data.abstractNote}}</div>

					<!--  si plus de 30 mots dans un toggle -->
					{% else %}
						<details>
							<summary>

								<!--  toggle fermé : afficher les 150 premiers caractères -->
								résumé : <span>{{doc.data.abstractNote|truncate(150)}}</span>
							</summary>
							<div>{{doc.data.abstractNote}}</div>
						</details>

					{% endif %}
			</div>
			{% endif %}
```

## Mes débuts en Javascript
En commençant à manipuler le langage Javascript, dont j'ignorais tout en arrivant au stage, j'ai eu pour réflexe d'écrire en commentaire chacune de mes actions pour comprendre ce que le code produisais. Cette surexplicitation m'a permis de prendre en main la complexité de la syntaxe en l'agrémentant de mes propres compréhensions.

*Code de mon premier script en Javascript (les commentaires sont les lignes précédées de //)*

``` javascript
// variable pour les boutons tags - filtre de navigation
var TAGS_NAV = document.querySelectorAll('.texte_tag_filtre')

// variable pour chaque document
var DOCS = document.querySelectorAll('.document')

// variable pour chaque tag de chaque document
var ITEM_TAGS= document.querySelectorAll('.tag')

// variable pour chaque document qui n'a pas de tags
var NOTAGS= document.querySelectorAll('.notags')

// variable pour se savoir ce qui était sélectionné avant
var selecPrecedente

// pour chaque bouton de filtre
TAGS_NAV.forEach(function(filtre, i) {
	// quand clic sur un bouton filtre
	filtre.addEventListener('click', function() {

  // désélectionner les filtres précédents (on ne peut avoir qu'un seul filtre à la fois)
  document.querySelectorAll('.tag_selection').forEach(function(selected){
    selected.classList.remove('tag_selection')
  })

  // si le filtre est déjà activé, le désactiver
    if (selecPrecedente == filtre) {
      filtre.parentElement.classList.remove('tag_selection')
      DOCS.forEach(function(doc) {
        // on réaffiche tout ce qui était caché
        doc.classList.remove('desactive')

      })

      // reset la sélection
      selecPrecedente = ''

  } else {
    // enregistre ce qui a été sélectionné
    selecPrecedente = filtre

    // rajoute un style "sélectionné" sur le bouton, ou l'enlève s'il est déjà là
    filtre.classList.toggle('tag_selection')

    // variable texte_filtre contient le texte de ce bouton
    var texte_filtre = filtre.innerHTML

    // cache les documents sans tags
    NOTAGS.forEach(function(notags) {
      notags.parentElement.classList.add('desactive')
    })

    // pour chaque tag de document
    ITEM_TAGS.forEach(function(item, i){
       // incluant la variable texte_filtre
      if (item.innerHTML.includes(texte_filtre)){
        // enlève la classe desactive
        item.parentElement.parentElement.classList.remove('desactive')

      }
      else {
        //si non attribue la classe desactive
        item.parentElement.parentElement.classList.add('desactive')

      }
    })
 }
})
})
```

## Un outil en plusieurs morceaux
L'outil de bibliothèque numérique pirate que nous développons se construit en plusieurs fichiers-modules qui ont chacun leur fonction technique. Ils sont organisés dans deux dépôts différents. Le premier contient le module python, base de la machine ainsi qu'un exemple de script qui s'en sert. Le deuxième contient les fichiers propres à l'interface de Maxisource (la bibliothèque pirate de Maxima), avec un script et des templates spécifiques. La séparation des fichiers dans ces deux dépôts permet à une personne extérieure qui voudrait ne récupérer que la machine de base de le faire sans avoir à faire le tri avec le reste du projet.

*Schéma des différents fichiers-modules de Maxisources*

![Schema](img/schema-fonctionnement.png)

<!-- ## Garantir l'autonomie
programme de Maxima : un outil utilisable en autonomie sans avoir à rentrer dans le code. -->