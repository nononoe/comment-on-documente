# Texte d'intention

![bienvenue](img/bienvenue-felicie.png)

*Extrait d'un pad de ma première réunion*

## Contexte(s) du stage
Mon stage s’est déroulé dans le collectif associatif Luuse. Luuse travaille le medium numérique pour produire des outils d’édition, d’écriture, de publication et d’archive. Leurs outils sont utilisés dans des cadres associatifs, culturels, pédagogiques et artistiques. Iels cultivent une approche de recherche expérimentale et critique, fortement rattachée philosophiquement et politiquement à la culture libre, dont iels reprennent tant les valeurs que les outils.

Pendant mon stage, j’ai intégré deux projets de recherche auto-initiés par Luuse, dans le cadre de l’axe de recherche « Formes d’archives ».

Le premier est la conception et la fabrication d’un numérisateur de livre (bookscanner). Ce projet reprend partiellement des plans partagés en ligne au sein d’une communauté d’amateur·ices. Un de nos objectifs est de s’assurer que l’objet final soit facilement démontable et transportable afin d’être utilisé en atelier. Pour cette raison, ainsi que par adhésion aux pratiques de diffusion qui existent au sein de la communauté en ligne, il était important pour nous de documenter et rendre accessibles les plans et la documentation de notre projet.

*Le bookscanner en action*

![Bookscanner](img/bookscanner.gif)

Le deuxième projet dans lequel je me suis investi est la conception d’un outil pour une bibliothèque numérique pirate. Cette bibliothèque repose sur un serveur accessible à travers un réseau local. Ce projet demande de créer et de gérer une base de données, ainsi que de concevoir une interface de consultation. L’interface que nous produisons est pensée pour une première implémentation de l’outil dans Maxima, le lieu d’occupation temporaire(1) dans lequel Luuse a ses bureaux. L’outil final vise également à être récupéré partiellement ou totalement, dans d’autres contextes comme pour des ateliers ou dans des lieux communautaires. Cette récupération peut s’effectuer à différents niveaux de la machine. Pour cela, nous mettons en place une documentation des différents éléments qui le composent.

*Formulaire de dépot de document*

![Maxisources](img/maxisources.png)

À côté de ces deux projets principaux, j’ai participé au développement d’un générateur pour une affiche de programmation de Maxima. Situé à Forest, commune de la région Bruxelles-Capitale, ce lieu est une initiative de l’association Communa et fonctionne en autogestion.  Cet outil doit permettre aux habitant·es de Maxima de générer l’affiche en remplissant les informations des évènements dans un tableur en ligne, sans jamais avoir à entrer dans le code. Ce principe demande également un travail de documentation et de guide pour assurer l’autonomie de l’outil.

En étant à Bruxelles, j’ai découvert un réseau de chercheur·euses praticien·nes duquel Luuse est proche. J’ai participé à un premier atelier du [projet ATNOFS](https://vvvvvvaria.org/en/atnofs-varia.html) (initié par divers collectifs dont le collectif Varia, à Rotterdam). Ce projet s’attelle à définir ce que serait un serveur informatique vu à travers une perspective féministe intersectionnelle. L’atelier fut l’occasion de rencontrer les réflexions qui travaillent ces chercheur·euses, et leurs pratiques de la recherche.

Pour construire ce rapport de stage, je me suis intéressé aux pratiques et aux enjeux de la documentation, dans ces contextes spécifiques.

## Définir la documentation
Pendant mon stage, j’ai observé des pratiques de documentation qui se mettent en place par des moyens techniques et humains. Chez Luuse, ces pratiques reposent en grande partie sur des outils numériques comme Git (2), mais également sur l’annotation, l’écriture ou le croquis. Quelque soit l’outil utilisé, la documentation engage des moyens humains, au sens d’une organisation de plusieurs personnes autour d’un dispositif technique qui accueille les échanges.

La documentation a une importance particulière vis-à-vis de la culture libre dans laquelle Luuse s’inscrit. Les valeurs de la culture libre reposent sur le développement d’outils modifiables et distribuables librement, ainsi que la mise en commun de savoirs et de productions culturelles. Parce qu’elle constitue un support d’échanges humains (3), la documentation est essentielle au développement de pratiques libres. Pour ce qui est des outils numériques, elle permet d’agrémenter la complexité abstraite d’un code par du texte et des représentations, et donne ainsi un point d’accroche à une personne extérieure.
J’ai cherché à identifier les formes que pouvait prendre la documentation et les enjeux qu’elle recouvrait, dans les démarches qui reposent sur les valeurs du libre.

À partir de mes observations, j’ai identifié trois enjeux principaux. Tout d’abord, la documentation occupe une place essentielle pour autoriser la [réappropriation d’un outil](reappropriation.md) par une personne extérieure (non-auteure). Par ailleurs, la documentation est un élément structurant du [travail collectif](travail-collectif.md). Elle est un support d’échange entre les acteur.ices du projet et permet l’identification des individualités. Enfin, la documentation, telle qu’elle s’inscrit dans une démarche de recherche expérimentale constitue un [support de restitution](restitution-recherche.md). Elle archive le processus de recherche, et par un travail de mise en lisibilité, le présente et le synthétise.

## Choix méthodologiques et formels
Pour restituer mes observations, j’ai choisi d’utiliser des méthodes propres à la pratique du numérique que j’ai découvertes avec Luuse. J’ai structuré ce rapport de stage sur un dépôt Git (un dossier rassemblant l’ensemble des fichiers d’un projet). La plateforme Gitlab (qui héberge le logiciel Git) m’a permis de générer un site web à partir du contenu de ce dépôt. Ce site est organisé autour des trois enjeux de la documentation, que je décrivais plus haut. Le modèle du site a été produit avec l’outil Mkdocs qui est spécialement prévu pour rassembler des éléments de documentations dans un template préconçu et modifiable.

## Les pratiques observées

### Commenter
Le commentaire est un texte intégré dans le code, que l’ordinateur n’exécute pas. Il est différencié du reste par des signes typographiques propre à chaque langage. Par exemple, un commentaire en langage html est encadré des signes `<!--` et `-->`. L’usage le plus répandu du commentaire dans un code fini consiste à expliquer la machine à l’endroit même où elle se trouve. Cela permet à une personne qui ne connaît pas le code de comprendre la structure de l’objet et la fonction de chaque partie. Au cours du développement d’un outil, le commentaire peut également servir à mettre un morceau de code de côté. En l’encadrant des signes typographiques appropriés, on le désactive. Cela permet de demander temporairement à l’ordinateur de l’ignorer, tout en le conservant à l’endroit où il pourrait encore être utile.

L’outil Git engage la production d’une autre forme de commentaire, au sens d’un texte qui accompagne en se plaçant à côté d’un objet. Le fichier ReadMe, généré automatiquement lors de la création d’un dépôt Git, a une fonction de page d’accueil. Un ReadMe explique à quoi sert le projet, comment il est structuré et comment l’utiliser. Lorsqu’on fait une contribution (un commit), Git demande également d’intégrer un message dans lequel on explique ce que contient la contribution. Le soin apporté à ce message permet de visualiser l’avancement du projet et facilite les éventuels retours sur l’historique des modifications.

### Organiser et nommer
La façon dont on nomme et on organise les fichiers et les dossiers a une fonction de documentation, car elle permet de préciser la fonction de chaque élément. Cela permet notamment à une personne qui souhaite récupérer un élément d’une machine de faire le tri entre ce qui est utile de garder ou pas. Au sein du code, le nom qu’on donne à des éléments comme les variables ou les classes impacte leur compréhension, et la facilité qu’on peut avoir à s’y référer.

### Prendre des notes
L’usage de pads comme support minimaliste d’écriture collective (4) est un moyen de documentation de moments de réunions et d’échanges. J’ai observé leur fonction essentielle au sein de Luuse et pendant l’atelier du projet ATNOFS. Comme objets de références, ils permettaient d’organiser la pensée et le travail collectif par le texte.

### Rendre visible
À plusieurs moments, nous avons eu l’usage de diagrammes et de visualisation pour produire un support d’échange. Ils avaient pour fonction de rendre concret et palpable un cheminement sur une interface ou l’organisation de concepts entre eux.

## Conclusion
Construire mon rapport de stage autour des pratiques de documentation m’a permis de comprendre leurs enjeux au regard des valeurs politiques portées par Luuse et les pratiques de la culture libre en général. Il me paraît essentiel d’intégrer cette approche de la documentation dans mon travail à venir ; par engagement pour la mise en commun des outils et des ressources, mais également comme méthodologie de recherche expérimentale.

Vis-à-vis de mon projet de recherche, mon stage m’a apporté des compétences techniques en code, mais également des modes de pensée et méthodes de travail qui s’y rattachent.

Mon projet de recherche s’intéresse à la mise en narration des outils numériques pour permettre leur réappropriation technique et politique. J’ai décidé pour cela de produire un outil de narration expérimentale sur la base du logiciel Git. En comprenant les logiques de manipulation des données, de récupération et de réadaptation d’outils, j’ai pu envisager avec plus de clarté les possibilités expérimentales de ce projet. D’un point de vue méthodologique, j’ai également pu prendre en main et expérimenter la démarche de conception d’une interface graphique : réfléchir au parcours de l’utilisateur·ice dans l’outil, penser un principe d’affichage des données à l’écran.

Enfin le contexte bruxellois m’a permis de rencontrer des collectifs et des lieux qui travaillent sur des enjeux similaires aux miens, que ce soit dans une démarche de recherche expérimentale ou conceptuelle, ou dans des cadres associatifs et pédagogiques.

***

(1) Cette pratique, répandue à Bruxelles, repose sur un contrat avec les institutions publiques de la ville pour l’occupation de lieux temporairement inutilisés.

(2) Git est un logiciel utilisé pour développer collectivement des outils numériques, afin de gérer les différentes versions du code et de synchroniser le travail entre plusieurs ordinateurs.

(3) Anne Cordier, « Quand le document fait société », Communication langages, 13 mai 2019, vol. 199, no 1, p. 21‑35.

(4) Cristina Cochior et Manetta Berends, « Minimal viable learning: How can minimal technology deliver maximum learning experiences? » dans Reclaiming Digital Infrastructures, Bruxelles, Books with an Attitude, 2021, p.

***

Crédits typographiques : [Victor Mono](https://rubjo.github.io/victor-mono/), Rune B