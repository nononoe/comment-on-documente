# Travailler ensemble

## Supports de conversation
À mon arrivée à Luuse, j'ai commencé à travailler sur le projet d'outil pour une bibliothèque pirate numérique qui était avait été partiellement abandonné. Je me suis basé sur les explications des personnes qui avaient travaillé dessus ainsi que sur une maquette de principe sur le dépôt du projet.

*Première maquette de l'interface à mon arrivée à Luuse*

![Maquette de l'interface V1](img/parcours-interfacev1.png)

De l'avis des autres personnes de Luuse, cette maquette comportait quelques défauts d'accessibilité et d'économie. J'ai commencé à repenser [une nouvelle maquette avec l'outil hotglue](https://felou.hotglue.me/?ressourcerie), qui reprenait dans les grandes lignes cette première maquette. Ces visuels ont été des supports d'échanges autour du parcours des utilisateur·ices de l'interface. Nous avons repris, découpé et réarrangé plusieurs fois sur un grand support qui nous permettait de **visualiser** le parcours imaginé et de le **présenter** aux autres personnes de Luuse.

![Parcours de l'interface](img/parcours-interface.jpeg)

## Les messages laissés
Les commentaires peuvent servir à se laisser des messages au sein du code. Ici, nous avions écrit des morceaux de code en python, dans un fichier sur lequel quelqu'un d'autre travaillait. Ce message indiquait à quoi ce code servait.

```python
# fausses routes pour test css :
@app.route('/collection')
def collection():
   rows = bk.getAllBooks()
   return render_template("collection.html", rows=rows)

@app.route('/create_collection')
def create_collection():
   return render_template("createcollection.html")

@app.route('/chercher')
def chercher():
   return render_template("chercher.html")

@app.route('/resultats')
def resultats():
   rows = bk.getAllBooks()
   return render_template("resultats.html", rows=rows)
```

## Gérer les conflits
Le logiciel Git permet de gérer les **conflits de versions**, c'est-à-dire les incompatibilités entre les différentes versions du projet sur l'ordinateur de chacun·e. Quand nous travaillons sur un projet, une copie est faite localement sur notre ordinateur et Git permet de les synchroniser. Des **conflits** arrivent quand nous avons travaillé sur la même partie du code et qu'un **merge** (une fusion) des versions n'est pas possible autonomatiquement.

*Échange de messages avec Thomas suite à un merge de version chaotique*

![Gérer les conflits](img/conflits.png)

## Partager un espace
Le moment de l'atelier ATNOFS fut un moment de réflexion autour de ce que peut être un serveur informatique, à travers une perspective féministe intersectionnel. Cela souleva des enjeux liés au langage utilisé (et la création d'**alias** pour les commandes), du partage de l'espace, du consentement.

*Extraits d'un pad : réflexions autour des termes des lignes de commande*

```
An attempt to write up what would be different ways to talk to type with or against bash / terminal commands

here's a list with bash commands: https://ss64.com/bash/
why is a cheat sheet called a cheatsheet ?

Bash as a term is not very social 
    
different methods to expending the narrative: 
* we could think of 'nar' pages next to manual: narrative pages in which the narration around a command is spelled on.
Or a 'gen' page: geneology of naming ? where the word comes from 
different meanings of the word

typing 'man' gives:
What manual page do you want?
Eliza psychtherapist version: 
    Are you sure you want a manual page ?

suggestive layer, not changing implying that we now better, but rather giving more context
'touch': 
    "did you ask consent before touching" ?

man man :

* are there more questions embedded in a manual page ? we can add to that / change it
* color as a sign that a command is an alias or an original command


bash
----
Thompson shell
bourne shell
bourne again shell
why shell?

'cat' in some distro's is called 'dog'
cat comes from catenation, chaining to come together
if dog is now a verb, dogination

--
sudo 'root'
'superuser' regular user versus superpowers
the process to access this superpowers includes
receiving the 'lecture' ( great powers great responsibilities, etc)

---

rm: remove .. where to ? (different than "delete")
-- information stays written on the disk, but nothing in the system knows anymore that there was a file: you erase the map but the house is stille there
remove visibility / from the map

mv: move and rename a file in one go
unlike in the gui, moving and renaming are the same thing, which re-organizes the understanding of what is a 'path'..

man: help manual
---
too many layers: the immediate masculinize meaning of man, the maspleaning tone which these manuals too often have

trap: Execute a command when the shell receives a signal
or: "nothing appropriate"
---
??

true: do nothing, successfully
---
the production of truth is not nothing, but rather quite active ;)

touch: 
evidence, when did you touch this last time
touch time
used to see if you have access to a file, permission boundaries, asking 
already crossing a boundary touch first, consent later?

for exmaple:
    --
    alias touch="echo 'would be nice to ask before touch'; touch"


sudo :
    temporary root privileges
    completey opposite meaning "super" (above ?) and "root" (ground)
```

*Fichier de configuration d'alias : donner de nouveaux noms aux commandes*

![Alias](img/alias-ATNOFS.png)

*Un bot nous rappelait toutes les demi-heures de prendre une pause*

![Take a break](img/take-a-break-ATNOFS.png)

*La fonction `<fold>` dans le chat d'un pad pour partager les réflexions d'un groupe au reste de l'atelier*

![Pads](img/pads-ATNOFS.png)

![Fold](img/foldATNOFS.png)