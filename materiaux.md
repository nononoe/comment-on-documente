# materiaux d'observation
ces 3 parties structurent ce que j'ai observé des pratiques de documentation à Luuse.  
3 pages qui rassemblent chaque élément (site web, photos, bout de code, pdf, scans de carnet) avec un texte d'introduction, qui décrit les enjeux.

## documentation pour la mise en commun, permettre réappropriation
- organisation d'un répo : faire un bon ReadMe qui explique à quoi sert l'outil et comment s'en servir. organisation de fichiers permettant la réappropriation partielle (ex: Maxisources -> module python + template + fiche de styles)
- usages du commentaire dans le code pour indiquer des informations. expliquer son propre code au moment où on le fait (mes début en javascript)
- site pour Bookscanner : plans
- programme de Maxima : un outil utilisable en autonomie sans avoir à rentrer dans le code.

## documentation pour le travail collectif
- mind map : support de conversation
- pads : historique, couleurs (écritures collectives)
- commentaires dans le code : garder des choses de côté
- conflits sur git : quand on modifie en parallèle. répartition du travail au préalable, communication par d'autres canaux. __gitignore__ : sélection de ce qui est pertinent de mettre en commun. __messages de commit__ ; __issues__

## documentation pour la restitution de la recherche
- pads : production d'un objet texte (octomode à ATNOFS)
- mind map : visualiser les enjeux
- site web http://archivism.luuse.io/ (important pendant la présentation à l'ERG)
- documentation du Bookscanner : restituer le processus de recherche, les tests (journal de bord)
- page de bibliographie Zotero

<!-- ## construction du Bookscanner
mettre en ligne des plans, les recherches. rassembler sur http://archivism.luuse.io/ -> faire le lien entre plusieurs projets.  
présentation à l'ERG pendant
### besoins
aller prendre des photos à l'IMAL (cet après-midi)

## rassembler des ressources
static zotero https://gitlab.com/Luuse/recherche_formes-archives/www.piraterie-et-biens-communs
module permettant de produire une page bibliographique à partir de l'API de Zotero.  
commentaires nécessaires pour que le code soit réappropriables facilement. bonnes pratiques de documentation.

## Maxisources
processus de production d'une interface  
écriture collective sur le pad, historique.

## ATNOFS
utilisation d'octomode comme moyen de rentre les pads restituables. -->